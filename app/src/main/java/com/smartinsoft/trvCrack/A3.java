package com.smartinsoft.trvCrack;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Random;

public class A3 extends AppCompatActivity {
    String[] soru={"Scooby Doo nun sahibi kimdir?",
            "Bruce Bannerın Hulka dönüşmesine sebep olan şey nedir?",
            "The Rocket Richard Mauricein meşhur olduğu tek ülke hangisidir?",
            "90lı yıllara damga vuran Bizimkiler dizisinde penceresinden sarkıp gelene geçene laf atan, sarhoş, karısının adı Sevim olan karakterin adı nedir?",
            "Pusula ve harita yardımıyla hedef bulma oyunu nedir?",
            "Aşağıdaki filmlerden hangisi bir oyundan uyarlanmıştır?",
            "Ninja kaplumbağaların eğitmeni kimdir?",
            "Yabancı Damat dizisindeki damadın ülkesi hangisidir?",
            "Super Mario Bros. dizisindeki Yoshi hangi renktir? Yoshi'nin rengi nedir?",
            "İskambilde hangi kart en büyüktür?",
            "Tarihte ruj ilk defa nerede kullanıldı? Tarihte ruj kullanan ilk millet hangisidir?",
            "Tarzan kiminle evlidir?",
            "Dünaydaki en yüksek beşeri (insan yapımı) yapı hangisidir?",
            "Hangisi bir su markası değildir?",
            "Piyanoda kaç tane beyaz tuş vardır? Piyanodaki beyaz tuş sayısı kaçtır?",
            "Hangi ülke en çeşitli orkide topluluğuna sahiptir? En çeşitli orkide topluluğuna sahip ülke?",
            "Hangi süper kahraman kara şövalye olarak düşünülmüştür?",
            "Shrekin yüzsüz ve en yakın arkadaşı kimdir?",
            "Satranç oyununda her zaman kim başlar? Satrançta oyuna kim başlar?",
            "Breaking Bad dizisindeki Jesse in soyadı nedir?",
            "Hangi Disney kahramanı camdan bir ayakkabı giyer?",
            "Garfieldin en iyi arkadaşının adı nedir? Garfieldin en yakın arkadaşı kimdir?",
            "Azerbaycan para birimi nedir?",
            "Köpek isimlerinden hangisinin animasyonu çekilmemiştir?",
            "Hangi ülke Viyana Şnitzeli ile ünlüdür? Viyana Şnitzeli yle ünlü ülke hangisidir?",
            "Geleceğe Dönüş filminde Marty McFlyın babası kimdir?",
            "4 tane iç içe geçmiş halka hangi araba markasını temsil eder?",
            "Hangi ülke suşiyi icat etmiştir?",
            "Balede tayt, mayo gibi giyilen özel eteğin adı nedir?",
            "Vosvos u üretim haline kavuşturan detaylarını kim tasarlanmıştır?",
            "Harry Potter serisi kitaplarında Hogwartsa katılan Nymphadora Tonks hangi gruba dahil olmuştur?",
            "Beatles hangi şehirden çıkmıştır?",
            "The Simpsonsdaki barmenin adı nedir?",
            "Grimm kardeşlerin isimleri nedir?",
            "Hangi ülkede yemek yedikten sonra geyirmek ayıp karşılanmaz?",
            "Ayakkabı giymemize yardımcı olan araca ne ad verilir?",
            "En fazla animasyon şirketine sahip ülke hangisidir?",
            "Hangisi Trivia Crack oyununda bir kategori sembolü değildir?",
            "Hangi uygulamanın maskotu bir hayalettir? Maskotu hayalet olan uygulama hangisidir?",
            "Satrançta her zaman en ön sırada aşağıdakilerden hangisi başlar?",
            "Hababam Sınıfının Mahmut Hocasına hayat veren usta oyuncumuz kimdir?",
            "Kendi evinizi inşa etmek için plan dizayn etmek istiyorsanız hangi profesyonel meslekten yardım alırsınız?",
            "O Ses Türkiye yarışmasının birinci sezonu kazanan jürisi kimdir?",
            "Kırmızı ve beyaz hangi rengi oluşturur? Kırmızı ve beyaz karışımı hangi rengi verir?",
            "Barack Obamanın eşinin adı nedir?",
            "ABD başbakanının uçağının adı nedir?",
            "Uluslararası polis teşkilatının merkezi nerededir?",
            "Karayip Korsanları 1 filminde lanetli geminin adı nedir?",
            "Bacı kelimesi ne anlama gelir?",
            "Her yıl Nobel Barış Ödülünün kime verileceğine karar veren 5 kişilik komite, hangi ülkenin parlamentosu tarafından görevlendirilir?",
            "Hangisi uzak doğu dövüş sanatlarından biri değildir?",
            "Taş Devrinde Fred Çakmaktaşın eşi kimdir?",
            "Özgürlük heykelindeki (New York) ateş nerde bulunur?",
            "Bir olayı kutlamak veya eğlenmek için düzenlenen ziyafete ne denir?",
            "Hangi ülkenin tüm vatandaşları askerlik yapmak durumundadır?",
            "Hangisi Lana Del Rey Şarkısı Değildir?",
            "Hangisi bir sokak oyunu değildir?",
            "Sünger Bob un en yakın arkadaşı kimdir?",
            "Kılı kırk yaran kelimesi hangisi için kullanılır?",
            "Hannibal Lecter olarak bilinen oyuncu kimdir?",
            "Disney Channel tarafından meşhur edilen ve 2013 te dağılan, New Jersey li üç erkek kardeşin kurduğu grup hangisidir?",
            "Mantı ile ünlü şehrimiz hangisidir?",
            "Tom ve Jerry nerede yaşıyor?",
            "Bilgisayar klavyesinde Q ve E tuşlarının arasında hangi tuş vardır?",
            "Hangi otomobil üreticisi italyandır?",
            "Hangi film serisinde Alaycı Kuş önemli bir rol oynar?",
            "Hangi marka Konya menşeilidir?",
            "Buz Devri Filminde Konuşan Tembel Hayvanın ismi nedir?",
            "Amerika nın hangi şehri en eski metro sistemine sahiptir?",
            "Te Amo nun anlamı nedir?",
            "Dünyaca ünlü araba markası Lamborghinin simgesi hangi hayvandır?",
            "Hangi oyuncu Spiderman karakterini oynamamıştır?",
            "ilk M-Rated video oyunu hangisidir?",
            "Hangi yıldızın adı Hollywood kaldırımında olması gerekirken sadece onun adı duvardadır?",
            "Survivor All Star yarışma programının şampiyonu kim olmuştur?",
            "Kurtlar Vadisi ilk defa kaç yılında yayınlanmıştır?",
            "Frank Zappa kimdir?",
            "Hangisi Türkiye Cumhuriyeti sınırları içerisinde havalimanı değildir?",
            "Bateri çalmak için kullanılan aletin adı nedir ?",
            "Hababam sınıfında Necmi nin lakabı nedir?",
            "Hangisi Cem Yılmaz ın yönetmenliğini yaptığı bir film değildir?",
            "Santrançta kaç beyaz fil taşı vardır?",
            "Avrupa Yakası dizisinde Engin Günaydın ın rol aldığı karakter kimdir?",
            "Hangi makyaj malzemesi yanaklara renk verilmesi için kullanılır?",
            "Hangi siyasetçi inşaat mühendisidir?",
            "Can Bonomo nun Eurovision a katıldığı şarkı hangisidir?",
            "Hangi sanatcı/grup eurovision da ülkemize 2.lik getirmiştir?",
            "Hangi video 2013 te tüm zamanların en çok izlenen YouTube videosu ünvanını kazanmıştır?",
            "Bangır Bangır Şarkısı sanatçıların hangisine aittir?",
            "Hangi boksör kelebek gibi uçarım arı gibi sokarım sözüyle ünlüdür?",
            "Aşağıdaki dizilerden hangisi havuç adlı karakterin oynadığı Türk dizisidir?",
            "Aşağıdakilerden hangisi bir Teoman parçası değildir?",};
    String[] cev ={"Shaggy",
            "Gama radyasyonu",
            "Kanada",
            "Cemil",
            "Oryantring",
            "Resident Evil",
            "Splinter",
            "Yunanistan",
            "Yeşil",
            "As",
            "Eski Mısır",
            "Jane",
            "Dubai-Burj Khalifa",
            "Tobleron",
            "52",
            "Kolombiya",
            "Batman",
            "Eşek",
            "Beyazlar",
            "Pinkman",
            "Kül kedisi - Sindirella",
            "Odie",
            "Şirvan",
            "Old Yeller",
            "Avusturya",
            "George McFly",
            "Audi",
            "Japonya",
            "Tütü",
            "Adolf Hitler",
            "Hufflepuff",
            "Liverpool",
            "Moe Szyslak",
            "Wilhem ve Jacob",
            "Hindistan",
            "Kerata",
            "Japonya",
            "Harita",
            "Snapchat",
            "Piyonlar",
            "Münir Özkul",
            "Mimar",
            "Murat Boz",
            "Pembe",
            "Michelle Obama",
            "Air Force One",
            "Fransa",
            "Kara inci",
            "Kız kardeş",
            "Norveç",
            "Kraw maga",
            "Vilma",
            "Sağ elinde",
            "Şölen",
            "İsrail",
            "Pure Love",
            "Tombala",
            "Patrick",
            "Fazla titiz",
            "Anthony Hopkins",
            "Hanson",
            "Kayseri",
            "Bilinmiyor",
            "W",
            "Alfa Romeo",
            "Açlık Oyunları",
            "Torku",
            "Sid",
            "New York",
            "Seni seviyorum",
            "Boğa",
            "Dylan O Brien",
            "Mortal Kombat",
            "Muhammed Ali",
            "Turabi Çamkıran",
            "2003",
            "Müzisyen",
            "Ercan Havalimanı",
            "Baget",
            "Güdük Necmi",
            "Vizontele",
            "2",
            "Burhan Altıntop",
            "Allık",
            "Süleyman Demirel",
            "Love Me Back",
            "Manga",
            "Gangnam style",
            "Gülşen",
            "Muhammad Ali",
            "Çocuklar Duymasın",
            "Aşkın Gözleri",};
    TextView sekran;
    Button c1,c2,c3,c4,lf,rt;
    int sorular=0;
    InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        sekran=(TextView)findViewById(R.id.soru);
        c1 = (Button)findViewById(R.id.c1);
        c2 = (Button)findViewById(R.id.c2);
        c3 = (Button)findViewById(R.id.c3);
        c4 = (Button)findViewById(R.id.c4);
        rt = (Button)findViewById(R.id.rt);
        lf = (Button)findViewById(R.id.lf);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1312048647642571/1433938245");
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        ys();
        rt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                rt.setBackgroundResource(R.drawable.r2);
                lf.setBackgroundResource(R.drawable.l1);
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(soru.length-1<=sorular)
                {
                    sorular=-1;
                }
                sorular++;
                ys();
            }
        });
        lf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                rt.setBackgroundResource(R.drawable.r1);
                lf.setBackgroundResource(R.drawable.l2);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(sorular<=0)
                {
                    sorular=soru.length;
                }
                sorular--;
                ys();
            }
        });

    }
    public void ys()
    {
        sekran.setText(soru[sorular]);
        Random rt =new Random();
        int nb=rt.nextInt(4)+1;
        switch (nb)
        {
            case 1:{ c1.setText(cev[sorular]); c1.setBackgroundResource(R.drawable.dol); break;}
            case 2:{ c2.setText(cev[sorular]); c2.setBackgroundResource(R.drawable.dol); break;}
            case 3:{ c3.setText(cev[sorular]); c3.setBackgroundResource(R.drawable.dol); break;}
            case 4:{ c4.setText(cev[sorular]);  c4.setBackgroundResource(R.drawable.dol); break;}
        }
    }
}
