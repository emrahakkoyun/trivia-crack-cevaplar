package com.smartinsoft.trvCrack;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Random;

public class A5 extends AppCompatActivity {
    String[] soru={"Hangisi psikolojik bir rahatsızlıktır ?",
            "Bir insan kaç dişe sahip olur?",
            "Kumda en çok hangi mineral bulunur? Kumda en çok bulunan mineral nedir?",
            "2 tane sperm 2 tane yumurta ile döllenince ne oluşur?",
            "Vücudumuzun kas oluşumu için kullandığı madde hangisidir?",
            "Kuş bilimi hangisiyle adlandırılır? Kuş bilimi nedir?",
            "Periyodik tablodaki satırlara ne denir?",
            "Atom çekirdeği etrafında dönen birim hangisidir?",
            "Akrepler hangi kategoride yer alır?",
            "Kaplumbağalar kaç yaşına kadar yaşar?",
            "En çok kullanılan kalem türü hangisidir?",
            "İlk uyduyu kim fırlatmıştır?",
            "Hangi hormon metabolizmayı düzenler? Metabolizmayı düzenleyen hormon hangisidir?",
            "Tüberkülozun en yaygın yan etkisi nedir?",
            "En büyük etobur hayvan hangisidir?",
            "Twitter ın yaratıcısı kimdir?",
            "Sirke ve maya etkileşiminden sonra açığa ne çıkar?",
            "Uzaya çıkan ilk kadın aşağıdakilerden hangisidir?",
            "Flamingolar hangi renktir? Flamingoların rengi nedir?",
            "Ay tutulması ne zaman olur?",
            "Sıvılar içinde çözülebilen maddelere ne denir?",
            "Hangi element oda koşullarında sıvı halde bulunur?",
            "Sıcak su torbasının diğer adı hangisidir?",
            "İlk telefon hangi yıl icat edilmiştir?",
            "Gametler (yumurta/sperm) hangi vücut sistemiyle bağlantılıdır?",
            "Bir yıldızın yakıtı nedir? Yıldız yakıtı olarak bilinen element hangisidir?",
            "Kendi yiyeceğini üreten canlılara ne denir?",
            "Günümüzde insanoğlunun bildiği en kuvvetli doğal ya da sentetik fiber hangisidir?",
            "ilk hava balonu uçuşunda balonda hangi hayvanlar bulunuyordu?",
            "Hemophobia ne korkusudur? Hemofobi ne korkusudur?",
            "Ohm kanununa göre, voltaj bölü direnç ne sonuç verir?",
            "Toplama işleminde 0 (sıfır) nasıl bir elemandır?",
            "Örümcekler kaç bacaklıdır?",
            "DNAnın yapısı hangisine en yakındır?",
            "1000den fazla türü olan bu memelilerden hangisi, avını ültrasonik seslerden bulabilir?",
            "Hangi madde söndürücü olarak kullanılmaz? Söndürücü olarak kullanılmayan madde hangisidir?",
            "En gürültülü kara hayvanı hangisidir?",
            "Bir elektrik terimi olan AC nin açılımı nedir?",
            "Bitki hücrelerindeki hücre duvarının görevi nedir?",
            "Zigot ne demektir?",
            "AIDS hangi yolla bulaşır?",
            "Hangi hayvan geviş getirmez? Geviş getirmeyen hayvan hangisidir?",
            "Bir işçi arının ortalama ömrü ne kadardır?",
            "Gamma harfi hangi alfabeye aittir?",
            "Vücudun en temiz bölümü neresidir?",
            "Böceklerin geçirdiği başkalaşıma ne denir?",
            "İnsan vücudunun hangi organı idrar oluşumundan sorumludur? İdrar oluşumundan sorumlu organ hangisidir?",
            "Hücreleri ilk hangi bilim insanı gözlemlemiştir?",
            "Yeni doğan bebeklerin ten renklerini sarıya döndüren hastalık nedir?",
            "Paramedikler hangi meslek dalında çalışmaktadırlar?",
            "Yunuslar yiyeceklerini nasıl bulurlar?",
            "Hangi otomobil şirketi Fairlane modelini yapmıştır?",
            "Kan ve bileşenlerini inceleyen bilim dalı hangisidir?",
            "Zengin hastalığı olarakta bilinen eklemlerde ürat birikimi sonucu ağrılı ve acı veren hastalık hangisidir? Zengin hastalığı hangisidir?",
            "Güneş sistemi dışındaki ilk gezegen ne zaman keşfedildi? Güneş sistemi dışındaki ilk gezegenin keşfi?",
            "Çalışmalarıyla modern evrimsel biyolojinin temelini oluşturan 19. yüzyıl bilim insanı kimdir?",
            "ilk atom modeli hangi bilim insanına aittir?",
            "İnternetin ilk kullanılma sebebi nedir?",
            "Hangi mantar çeşidinden antibiyotik elde edilir?",
            "İçinden ışığın geçemediği objeye ne ad verilir?",
            "Hangi gezegen en geniş sıcaklık sınırına sahiptir? En geniş sıcaklık sınırına sahip gezegen hangisidir?",
            "Örtü sistemi nedir?",
            "Denizde bikini kasabasında yaşayan deniz yıldızının adı nedir?",
            "Klavikula ne demektir?",
            "Hangisi böbrekleri inceleyen bilimdir?",
            "Hangisi etobur değildir?",
            "Aşağıdakilerden hangileri 2 çeşit kimyasal bağ isimleridir?",
            "Eğer bir kadın 16 haftalık hamileyse kaç aylık hamiledir?",
            "Hangisinin çekim kuvveti daha fazladır?",
            "Aşağıdakilerden Hangisi Genetiğin Babası Olarak Bilinir?",
            "İnsanların önyargılarını parçalamak atomu parçalamaktan zordur sözü hangi ünlü bilim adamına aittir?",
            "Aşağıdaki canlılardan hangisinin midesi yoktur?",
            "1908de Alva Fisher elektrikli çamaşır makinesini icat ettiğinde makineye ne ad verilmiştir?",
            "Mendel Kalıtım ile ilgili deneyde Hangi bitkiyi Kullanmıştır?",
            "Havuzu bakterilerden korumak için havuza konulan kimyasalın adı nedir?",
            "Göçmen kuşların uçuş şekli hangisidir?",
            "Vücudu enfeksiyonlara karşı koruyan kan hücresinin adı nedir?",
            "DNAnın dört bazı hangileridir?",
            "Beş duyulardan hangisi köpekler için en yararlıdır? Köpeklerin en yararlı duyusu nedir?",
            "Bebeklerin gözleri ilk başta (ilk doğduklarında) genellikle hangi renktedir?",
            "Hangisi daha çok tüketiliyor?",
            "Radyasyonun ana kaynağı nedir?",
            "Aşağıdakilerden hangisi bir iç kuvvet türü değildir?",
            "Diyaframda istenmedik bir daralmaya ne denilir? Diyaframdaki istenmedik daralma nedir?",
            "Kalbin kendi kas dokusunu besleyen damarlara ne ad verilir?",
            "Hangi hayvan kanser olmamasıyla bilinir? Kanser olmayan hayvan hangisidir?",
            "Güneşin, galaksinin merkezinde olduğu teorisini kim ifade etmiştir?",
            "Karbonun proton sayısı kaçtır? Karbonda kaç proton vardır?",
            "Linux un tanımı nedir?",
            "Yeni doğmuş bir bebeğin en dominant algısı hangisidir?",
            "Gırtlağın ön ve alt bölümünde bulunan içsalgı bezine ne ad verilir?",
            "Kadınların regl döneminin halk dilindeki adı nedir?",
            "Hücrede enerji üretiminden sorumlu organel hangisidir?",
            "Bal peteklerinde bulunan her bir altıgene ne ad verilir?",
            "Şişe açacağını kim icat etmiştir? Şişe açacağının mucidi kimdir?",
            "Havai fişeklerdeki kırmızı renk hangi elementten elde edilir?",
            "1 eksiği 25 in karesi olan sayı hangisidir?",
            "Hücrenin enerji üreten organeli hangisidir?",
            "Yeşil bitkilerde görülen güneş ışığından kimyasal enerji dönüşümüne ne ad verilir?",
            "Pavlov Klasik Şartlanma deneyinde aşağıdaki hayvanlardan hangisini kullanmıştır?",
            "Hangisi Kan Glükozunu dengeleyen hormondur?",
            "İnsan vücudunun ana enerjik molekülü hangisidir?",
            "Bir DVD nin HD versiyonu nedir?",
            "Astrolojide, The Scorpio hangi sembolle temsil edilir?",
            "Fransiyumun kimyasal sembolü nedir?",
            "Hangi canlı türü havasız bir ortamda dahi yaşamını devam ettirebilir?",
            "Albino hastalığı ne demektir?",
            "Sodyum Hidroksit Nasıl Yazılır?",
            "Hangisi bir göz kusuru değildir?",
            "Hangisi bir ağrı kesici değildir?",
            "Hangisi DNA da bulunmaz?",
            "Yazıyı bulan uygarlık hangisidir?",
            "Sodyum ve kloru karıştırırsan ne elde edersin?",
            "Hidrojen atomunda kaç proton bulunur?",
            "Cosx ın türevi nedir?",
            "Hangi hormon mutluluk hormonu olarak anılır?",
            "Hangi Kan Grubu herkese kan verebilir?",
            "Ahtopot un kaç kalbi vardır?",
            "Hangisi Piaget in gelişim evrelerinden değildir?",
            "Minare ve bayrak direklerinin tepesindeki hilale ne denir?",
            "Dünyadaki en büyük hayvan hangisidir?",
            "Kandaki şeker miktarını en çok hangi besin türü artırır?",
            "Kokusu, rengi ya da tadı olmayan öldürücü gaz hangisidir?",
            "Kurşun elementin periyodik tablodaki sembolü nedir?",
            "Halojenler periyodik tablonun kaçıncı grubunda bulunurlar?",
            "Jupiterin kaç uydusu vardır?",
            "Hangi pH seviyesi vücudumuz için en uygundur?",
            "Hangi veba felaketi Avrupa nın çoğunu esir almıştır?",
            "Aşağıdakilerden hangisi matematiksel bir terim değildir?",
            "İnsanlar çim biçme makinesinin icadından önce çimlerini kesmek için ne kullanırlardı?",
            "Einstein hangi çalışması ile Nobel ödülü almıştır?",
            "Solunumun yan ürünü nedir?",
            "Vücutta savaşçı kan olarak bilinen kan hücresi hangisidir?",
            "Minecraft hangi firmanındır?",
            "Kirpi hangi kategoriye aittir? Kirpinin kategorisi nedir?",
            "Menenjit vücudun hangi bölümünü etkiler?",
            "Hangisi biyotik (canlı) faktörlerden biri değildir?",
            "Merkürün kaç tane uydusu vardır? Merkür'ün uydu sayısı kaçtır?",
            "Hangisi beynin loblarından biri değildir?",
            "Buharın sıvıya dönüşmesi işlemine ne ad verilir?",
            "Asit ve bazın nötr olduğu pH numarası kaçtır?",
            "Yaprağı müshil olarak kullanılan bitki hangisidir?",
            "Dünyanın en uzun örümceği hangisidir?",
            "Aşağıdakilerden hangisi çekinik gen ile taşınır ?",};
    String[] cev ={"Şizofreni",
            "26",
            "Quartz",
            "Çift yumurta ikizi",
            "Protein",
            "Ornitoloji",
            "Periyot",
            "Elektron",
            "Eklembacaklılar",
            "115ten fazla",
            "Kurşun kalem",
            "Sovyet Rusya",
            "T3 (Triiyodotronin)",
            "Öksürükle kan gelmesi",
            "Kutup ayısı",
            "Jack Dorsey",
            "Karbondioksit",
            "Valentina Tereşkova",
            "Pembe",
            "Dünya güneş işle ay arasındayken",
            "Çözünen",
            "Cıva",
            "Termofor",
            "1876",
            "Üreme",
            "Hidrojen",
            "Ototrofik",
            "Örümcek Ağı",
            "Hepsi",
            "Kan korkusu",
            "Akım",
            "Etkisiz",
            "8",
            "Çift sarmal",
            "Yarasa",
            "Karbonmonoksit",
            "Uluyan maymun",
            "Alternatif akım",
            "Hücreyi korumak",
            "Döllenmiş Yumurta",
            "Kan ve cinsel",
            "At",
            "3 Ay",
            "Yunan",
            "Gözler",
            "Metamorfoz",
            "Böbrek",
            "Hooke",
            "Sarılık",
            "Sağlık",
            "Sesin yankılanmasıyla uzaklığı hesaplayarak",
            "Ford",
            "Hematoloji",
            "Gut",
            "1992",
            "Charles Darwin",
            "john dalton",
            "Askeri",
            "Küf mantarı",
            "Opak",
            "Merkür",
            "Deri",
            "Patrick",
            "Köprücük kemiği",
            "Nefroloji",
            "Boğa",
            "İyonik ve Kovalent",
            "4 ay",
            "Kara Delik",
            "Mendel",
            "Albert einstein",
            "Denizatları",
            "Thor",
            "Bezelye",
            "Klor",
            "V şeklinde",
            "Akyuvar",
            "A, T, C, G",
            "Koklamak",
            "Mavi",
            "Su",
            "Güneş",
            "Rüzgar",
            "Hıçkırık",
            "Koroner damar",
            "Köpek balığı",
            "Galileo",
            "6",
            "Bir işletim Sistemi",
            "Duyma",
            "Tiroit",
            "Ay Başı",
            "Mitokondri",
            "Gümeç",
            "William Painter",
            "Stronsiyum",
            "626",
            "Mitokondri",
            "Fotosentez",
            "Köpek",
            "insülin",
            "ATP",
            "Blu-Ray",
            "Akrep",
            "Fr",
            "Tardigrat",
            "Doğuştan beyaz olma",
            "NAOH",
            "Sinüzit",
            "Prozac",
            "Urasil",
            "Sümer",
            "Tuz",
            "1",
            "-Sinx",
            "Serotonin",
            "0",
            "Üç",
            "Fiziksel evre",
            "Alem",
            "Mavi balina",
            "Karbonhidratlar",
            "Karbonmonoksit",
            "Pb",
            "7",
            "67",
            "7",
            "Kara ölüm",
            "Akım",
            "Tırpan",
            "Fotoelektrik etkisi",
            "CO2",
            "Akyuvar",
            "Microsoft",
            "Memeli hayvanlar",
            "Beyin",
            "İklim",
            "Hiç yoktur",
            "Servikal",
            "Yoğunlaşma",
            "7",
            "Sinemaki",
            "Dev tarantula",
            "Yeşil göz",};
    TextView sekran;
    Button c1,c2,c3,c4,lf,rt;
    int sorular=0;
    InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a5);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        sekran=(TextView)findViewById(R.id.soru);
        c1 = (Button)findViewById(R.id.c1);
        c2 = (Button)findViewById(R.id.c2);
        c3 = (Button)findViewById(R.id.c3);
        c4 = (Button)findViewById(R.id.c4);
        rt = (Button)findViewById(R.id.rt);
        lf = (Button)findViewById(R.id.lf);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1312048647642571/4387404646");
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        ys();
        rt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                rt.setBackgroundResource(R.drawable.r2);
                lf.setBackgroundResource(R.drawable.l1);
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(soru.length-1<=sorular)
                {
                    sorular=-1;
                }
                sorular++;
                ys();
            }
        });
        lf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                rt.setBackgroundResource(R.drawable.r1);
                lf.setBackgroundResource(R.drawable.l2);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(sorular<=0)
                {
                    sorular=soru.length;
                }
                sorular--;
                ys();
            }
        });

    }
    public void ys()
    {
        sekran.setText(soru[sorular]);
        Random rt =new Random();
        int nb=rt.nextInt(4)+1;
        switch (nb)
        {
            case 1:{ c1.setText(cev[sorular]); c1.setBackgroundResource(R.drawable.dol); break;}
            case 2:{ c2.setText(cev[sorular]); c2.setBackgroundResource(R.drawable.dol); break;}
            case 3:{ c3.setText(cev[sorular]); c3.setBackgroundResource(R.drawable.dol); break;}
            case 4:{ c4.setText(cev[sorular]);  c4.setBackgroundResource(R.drawable.dol); break;}
        }
    }
}
