package com.smartinsoft.trvCrack;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Random;

public class A1 extends AppCompatActivity {
    String[] soru={"Hangi ressam genelde çalıntı eserleri resmetmiştir? Çalıntı eserleri resmeden ressam kimdir?",
            "Hangi filmin imbd puanı daha yüksektir ?",
            "2009 yılında dünyada en yüksek gişe hasılatı kazanan film hangisidir ?",
            "Renk kullanmadan çizilen resme verilen ad nedir?",
            "Romeo, Romeo, neden Romeosun sen? satırlarıyla ünlü oyunu kim yazmıştır?",
            "3 diyezli majör ton hangisidir?",
            "Türk edebiyatında ilk yazılı metin örneği aşağıdakilerden hangisidir?",
            "Müzikte Dur anlamına gelen söz hangisidir?",
            "Piyano kelimesinin gerçeği nedir?",
            "Hangisi dinden atma anlamına gelir?",
            "Sinema filmi çekerken kullanılan klaket neden siyah beyaz renktedir?",
            "Yıldız Savaşlarının ilk filmi ne zaman çekilmiştir?",
            "Türkiye'yi Eurovision'da temsil eden ilk sanatçı kimdir?",
            "İyi kötüdür, kötü ise iyidir sözü Shakespearein hangi oyununda geçmektedir?",
            "Bir gecede 6 Grammy ödülü kazanan ilk kadın şarkıcı kimdir?",
            "Hayat basittir ama biz hayatı zorlaştırmak için ısrar ediyoruz sözü kime aittir?",
            "İlk tarihi roman hangisidir? Edebiyatımızın ilk tarihi romanı hangisidir?",
            "Yüzüklerin Efendisi filminde aklı bir büyücü tarafından zehirlenen kral hangisidir?",
            "Game of Thrones adlı dizide Demir Bankası hangi Şehirdir?",
            "Yeni Kral Olmak İsteyen Adam kitabının yazarı kimdir?",
            "Alexandre Dumas'ın Üç Silahşörler adlı eserindeki dördüncü silahşörün adı nedir?",
            "Edebiyatta baskın karakter aynı zamanda ne şekilde adlandırılır?",
            "Hangisi Besteci Değildir?",
            "Müziğin Picassosu olarak bilinen Ocak 2016da ölen ünlü sanatçı kimdir?",
            "Hintli kadınların alınlarının ortasına yaptıkları boya neyi simgeler?,",
            "Tolkienin kitaplarında hangi hobbit ilk güç yüzüğünü bulmuştur? İlk güç yüzüğünü bulan hobbit kimdir?",
            "Yunanistanda yaşamış ilk fabl yazarı kimdir?",
            "Aşık Veysel hangi şehrimizin sanatçısıdır? Aşık Veysel nerelidir?",
            "Danny Boyle hangi filmiyle en iyi yönetmen Oscarını almıştır ?",
            "Hangi savaşçı sınıfı orijinal gizli görevleri yapmakla ünlüdür?",
            "Pastel boyaya benzeyen kuru uzun ve ince olan boya türünün adı nedir?",
            "Hangi karakter dev bir sarı kapsül şeklindedir? Dev bir sarı kapsül şeklindeki karakter hangisidir?",
            "Nazım Hikmet aşağıdaki gazetelerden hangisinde yazmıştır?",
            "Ünlü Hz. Davut heykeli hangi Rönesans dönemi sanatçısının eseridir?",
            "Kuran-ı Kerim sayfalarını süsleme sanatı hangisidir?",
            "Bad Blood şarkısını yazan ve söyleyen kimdir?",
            "Ressamların üzerine tuval veya duralit koyduğu 3 ayaklı tahtanın adı nedir?",
            "İlahi Komedyanın yazarı ünlü İtalyan şair Dantenin ölümsüz aşkının adı nedir?",
            "1954 Nobel Edebiyat Ödülünü hangi yazar kazanmıştır?",
            "Hangisi bir orkestradaki en geniş 2. (ikinci) telli çalgıdır?",
            "Nobel barış ödülünü kazanan en genç kişi kimdir?",
            "Müzikte asıl sese oranla inceltilmiş ses nedir?",
            "Hawaii nin geleneksel dansı nedir?",
            "Hangi Ressam Sıklıkla Eşinin Resimlerini Çizmiştir?",
            "Kadın vokal seslerinde en yüksek ses aralığı hangisidir?",
            "Kendisine at sineği lakabını takan filozof hangisidir?",
            "Bennet ailesi hangi Jane Austen romanında yer alır?",
            "Hangi ressam mavi dönemi yle bilinir?",
            "Alice Harikalar Diyarında sürekli meşgul olan karakter hangisidir?",
            "Leonardo da Vinci'nin ünlü Son Akşam Yemeği tablosunda masanın ortasında yer alan kişi kimdir?",
            "Luigi adında bir abisi olan dünyaca ünlü oyun karakterinin adı nedir?",
            "ŞEKER PORTAKALI Romanının Yazarı Kimdir?",
            "us edebiyatçı Dostoyevski hangi hastalığa sahipti? Dostoyevski ne hastasıydı?",
            "Sanatta tek bir parçayı konu alan akıma ne ad verilir?",
            "Bruce Leenin oluşturduğu dövüş sanatı hangisidir?",
            "Edebiyatta bilip de bilmemezlikten gelme sanatının ismi nedir?",
            "Thorun babası kimdir?",
            "Beatles grubunun hangi üyesi kendi çizimleriyle ünlü olmuştur?",
            "Müzikte notayı uzatmaya yarayan işaret nedir?",
            "RMS Titanicin yapımında en çok nereli işçi çalıştı?",
            "Hangi ünlü rapci yazdığı kitabın sayfalarını okuyucularının tütün sarmaları için sigara kağıdından yaptırmıştır?",
            "Hangi grup Get Lucky isimli şarkıyı Pharrel Williams ile ortak yazmıştır?",
            "Hangisi Dan Brown ın eserlerinden biri değildir?",
            "Ünlü balmumu müzesinin adı nedir?",
            "Simgesi kalp olarak bilinen ve genellikle klasik kitapları yayınlayan ünlü yayınevinin ismi?",
            "South Park dizisinde sürekli ölen karakter hangisidir? South Parkın sürekli ölen karakteri kimdir?",
            "Keman yayı neyden yapılır?",
            "Milli şairimiz Mehmet Akif Ersoyun mesleği nedir?",
            "Hangi Metallica şarkısının adı bir sayıdır?",
            "Winnie the Pooh hikayelerindeki Eeyore ne çeşit bir hayvandır?",
            "Çiftlerden hangisi halk edebiyatının efsane aşıklarından değildir?",
            "Türkiyenin Yaşayan ve en yaşlı olan fotoğraf sanatçısı kimdir?",
            "Yaptığı TV programında manzara resimleri çizen ressamın adı nedir?",
            "Hangi renk maviden yapılmaz?",
            "Tiyatro salonu gibi bir mekanın girişini tanımlayan mimari terim nedir?",
            "Mahatma Gandhi'nin Gerçekle Deneyimlerimin Hikayesi aslı eseri hangi türdedir?",
            "Rene Descartesin ünlü sözü hangisidir?",
            "Nibelungen Destanı hangi ülkeye aittir?",
            "Antik Yunan mitolojisindeki hangi karakter kafasında yılanlara sahiptir?",
            "Ray Bradburynin Fahrenheit 451 romanında, deniz kabukları tanım itibariyle hangi modern aygıta benzer?",
            "Hangi kitap yayınlandığında intiharların artmasına neden olmuştur? İntiharların artmasına neden olan kitap hangisidir?",
            "Ne olursan ol yine de gel diyen ünlü Türk mevlevi düşünürümüz kimdir?",
            "Hangisi gençleri bir arenada ölümüne savaşmaya zorlayan kitap serisidir?",
            "Hangisi resimde ana renk değildir?",
            "Quino tarafından yaratılan ünlü karakter kimdir?",
            "Mozart ve Hadyn hangi müzik döneminde popülerdiler?",
            "Harry Potterın kaç kardeşi vardır?",
            "Michael Jackson hangi yılda vefat etmiştir?",
            "Aşağıdakilerden hangisi Robin Sharma eseridir?",
            "Balede kullanılan ayakkabının adı nedir?",
            "Türk Edebiyatında öğretici şiirlere ne ad verilir?",
            "Big Bang Theory dizisindeki karakterlerinin mesleği nedir?",
            "Hangisi beş hececilerden biridir?",
            "İlk piyanoyu kim icat etmiştir? Piyanonun mucidi kimdir?",
            "Hangisi Tevfik Fikret in Çocuk Kitabıdır?",
            "Yazar Hüseyin Rahmi Gürpınar hayatının büyük bir bölümünü İstanbulun hangi adasında geçirmiştir?",
            "Stefani Joanne Angelina Germanotta kimdir?",
            "Hayko Cepkinin ilk albümünün adı nedir?",
            "Kitap Kadar Sadık Bir Arkadaş Yoktur. sözünü kim söylemiştir?",
            "Poyraz Karayel adlı dizide oynayan ve 2015 te Altın Kelebek ödülü kazanmış olan çocuk oyuncu kimdir ?",
            "Sürrealist olarak anılan Frida Kahlonun doğduğu ülke aşağıdakilerden hangisidir?",
            "The Godfather serisi kaç filmden oluşur?",
            "Kemandaki en ince sesi çıkaran telin adı nedir?",
            "Mai ve Siyah kimin eseridir ?",
            "Harry Potterdaki ev cininin ismi nedir?",
            "Shakespeare Hamlet i hangi dönemde yazmıştır?",
            "Mozart hangi ülkedendir?",
            "Arjantin in ulusal dansı nedir?",
            "Ana Renklerin Özelliği Nedir?",
            "Dört mevsim adlı senfoni hangi besteci besteciye aittir?",
            "Monet en çok neyin resmini çizmeyi severdi?",
            "Hangisi Fight Club (Dövüş Kulubü) Oyuncularından Biridir ?",
            "Hangisi resimle ilgili bir alet değildir?",
            "Hangisi George Orwellın ölümünden sonra, kitapları 1984 ve Hayvan Çiftliğinin yayın haklarını satın almıştır?",
            "Hangisi Marvel evreninde degildir?",
            "Ressam Renoirnın ilk adı nedir?",
            "Aşağıdaki enstürmanlardan hangisi ilk kez A.B.D. de üretildi?",
            "Aşağıdaki kitaplardan hangisi Reşat Nuri Güntekin in değildir?",
            "Hangi film kitaptan uyarlamadır?",
            "Nazım Hikmet Ran ın mezarı nerededir?",
            "Dadaizm ne demektir?",
            "Flüt hangi enstrüman kategorisinde yer alır?",
            "Hangi caminin minare sayısı 6 dır?",
            "Hangisi Uyumsuzun ikinci filmidir?",
            "Hangi sanatçı New York taki bir sanat sergisinde sanat eseri olarak bir tuvalet sergilemiştir?",
            "Hababam Sınıfı filmindeki Kel Mahmut rolünü kim oynamıştır ?",
            "Turuncu hangi renklerin karışımı ile olur?",
            "Hangisi ünlü bir seri katilin anlatıldığı dizidir?",
            "Renkli kağıtları yırttıktan sonra bu kağıtları bir kağıt üzerine yapıştırılarak yapılan çalışmanın adı nedir?",
            "Aşk-ı Memnu, Mai ve Siyah gibi eserlerin sahibi ünlü türk yazarı kimdir?",
            "Hangisi candan ercetin şarkısı degildir?",
            "Hangisi Yüksek Rönesans ın ustalarından biri olarak bilinmez?",
            "Hangisi kardeş payı dizisi karakterlerindendir?",
            "Üç Küçük Domuzcuk taki domuzcukların evleri hangi malzemelerden yapılmıştı?",
            "Hangisi küçük parçalarla düzenli olan bir resim adıdır?",
            "Vitruvius Adamı hangi ressamın eseridir?",
            "Hangisi bir Luc Besson filmidir?",
            "Yağlı boya herhangi bir yere bulaştığında nasıl çıkartılır?",
            "Hangisi bir Selami Şahin bestesi değildir?",
            "Hangisi bir Dostoyevski romani degildir?",
            "Olmak ya da olmamak hangi oyuna aittir?",
            "Yıldız Savaşlarının ilk filmi ne zaman çekilmiştir?",};
    String[] cev ={"Rembrandt",
            "Avatar",
            "Avatar",
            "Karakalem",
            "Shakespeare",
            "La majör",
            "Orhun abideleri",
            "Es",
            "Piyanoforte",
            "Afaroz",
            "Kameranın beyaz dengesini sağlar",
            "1977",
            "Semiha Yankı",
            "Macbeth",
            "Beyonce",
            "Konfüçyüs",
            "Cezmi",
            "Theoden",
            "Braavos",
            "Kipling",
            "Dartagnan",
            "Ana Karakter",
            "Monet",
            "David Bowie",
            "Evli olduklarını",
            "Bilbo",
            "Aesop",
            "Sivas",
            "Milyoner",
            "Ninjalar",
            "Carandache",
            "Minyon",
            "Aydınlık",
            "Michelangelo",
            "Tezhip",
            "Taylor Swift",
            "Şövale",
            "Beatrice",
            "Ernest Hemingway",
            "Çello",
            "Martin Luther King",
            "Diyez",
            "Hula",
            "Picasso",
            "Soprano",
            "Sokrates",
            "Gurur ve önyargı",
            "Pablo Picasso",
            "Tavşan",
            "Hz. İsa",
            "Mario",
            "Jose Maura De Vasconcelos",
            "Epilepsi",
            "Nokta sanatı",
            "Jeet Kune Do",
            "Tecaülü arif",
            "Odin",
            "John Lennon",
            "Legato",
            "İrlandalı",
            "Snoop Dog",
            "Daft Punk",
            "Olasılıksız",
            "Madam Tussaud",
            "Can Yayınları",
            "Kenny",
            "At Kuyruğu",
            "Baytar",
            "One",
            "Eşek",
            "Elif - Doruk",
            "Ara Güler",
            "Bob Ross",
            "Turuncu",
            "Fuaye",
            "Otobiyografi",
            "Düşünüyorum öyleyse varım",
            "Almanya",
            "Medusa",
            "Kulak içi kulaklık",
            "Genç Wertherin Acıları",
            "Mevlana Celaleddin Rumi",
            "Açlık Oyunları",
            "Yeşil",
            "Mafalda",
            "Klasik",
            "0",
            "2009",
            "Ferrarisini Satan Bilge",
            "Point",
            "Didaktik Şiir",
            "Bilim adamı",
            "Faruk Nafiz Çamlıbel",
            "Bartolomeo Cristofori",
            "Şermin",
            "Heybeliada",
            "Lady Gaga",
            "Sakin olmam lazım",
            "Ernest Hemingway",
            "Ata Berk Mutlu",
            "Meksika",
            "3",
            "Mi teli",
            "Halit Ziya Uşaklıgil",
            "Dobby",
            "Rönesans",
            "Avusturya",
            "Tango",
            "Diğer Renklerin karıştırılmasıyla elde edilmezler.",
            "A.Vivaldi",
            "Nilüfer tabloları",
            "Brad Pitt",
            "Kil",
            "CIA",
            "Batman",
            "Pierre",
            "Kazoo",
            "Masumiyet Müzesi",
            "John Green-Aynı Yıldızın Altında",
            "Rusya",
            "Sanat akımı",
            "Üflemeli",
            "Sultanahmet Camii",
            "Kuralsız",
            "Marcel Duchamp",
            "Münir Özkul",
            "Kırmızı Sarı",
            "Hannibal",
            "Kolaj",
            "Halit Ziya Uşaklıgil",
            "Sözüm ona sevdin",
            "Monet",
            "Ahmet kural-murat cemcir",
            "Çubuk",
            "Mozaik",
            "Da Vinci",
            "Leon",
            "Tiner",
            "Başım Belada",
            "Ölü Canlar",
            "Hamlet",
            "1977",};
    TextView sekran;
    Button c1,c2,c3,c4,lf,rt;
    int sorular=0;
    InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1312048647642571/6003738643");
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        sekran=(TextView)findViewById(R.id.soru);
        c1 = (Button)findViewById(R.id.c1);
        c2 = (Button)findViewById(R.id.c2);
        c3 = (Button)findViewById(R.id.c3);
        c4 = (Button)findViewById(R.id.c4);
        rt = (Button)findViewById(R.id.rt);
        lf = (Button)findViewById(R.id.lf);
        ys();
        rt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                rt.setBackgroundResource(R.drawable.r2);
                lf.setBackgroundResource(R.drawable.l1);
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(soru.length-1<=sorular)
                {
                    sorular=-1;
                }
                sorular++;
                ys();
            }
        });
        lf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                rt.setBackgroundResource(R.drawable.r1);
                lf.setBackgroundResource(R.drawable.l2);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(sorular<=0)
                {
                    sorular=soru.length;
                }
                sorular--;
                ys();
            }
        });

    }
    public void ys()
    {
        sekran.setText(soru[sorular]);
        Random rt =new Random();
        int nb=rt.nextInt(4)+1;
        switch (nb)
        {
            case 1:{ c1.setText(cev[sorular]); c1.setBackgroundResource(R.drawable.dol); break;}
            case 2:{ c2.setText(cev[sorular]); c2.setBackgroundResource(R.drawable.dol); break;}
            case 3:{ c3.setText(cev[sorular]); c3.setBackgroundResource(R.drawable.dol); break;}
            case 4:{ c4.setText(cev[sorular]);  c4.setBackgroundResource(R.drawable.dol); break;}
        }
    }
}
