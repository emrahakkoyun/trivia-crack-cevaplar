package com.smartinsoft.trvCrack;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Random;

public class A6 extends AppCompatActivity {
   String[] soru={"Arden Ormanları nerededir? Hangi ülkededir?",
           "Kama Nehri hangi ülkededir ?",
           "Java adası hangi ülkededir?",
           "Dünyadaki en büyük yarımada hangisidir?",
           "Ülkemizde vadilerden hangisinde ulaşım daha çok geçitler yardımıyla sağlanır?",
           "Her yıl en çok fırtınanın yaşandığı kıta hangisidir?",
           "Avrupa kıtasında toplam kaç ülke vardır? Avrupadaki ülke sayısı kaçtır?",
           "Aşağıdaki Güney Amerika ülkelerinin hangisinde 2010da 8.8 büyüklüğünde deprem meydana gelmiştir?",
           "Hangisi işsizlik oranının en yüksek olduğu ülkedir?",
           "Güneş sisteminde yer alan uydulardan hangisi yörüngesinde döndüğü gezegene kıyaslandığında en büyüktür?",
           "Antalyanın portakalı ile ünlü ilçesi hangisidir?",
           "Dünyada bilinen ilk kilise hangi şehirdedir? Dünyanın ilk kilisesi nerededir?",
           "Bangladeşin başkenti neresidir?",
           "Menderes Terimi nedir?",
           "Türkiyede en fazla ve en az yağış alan ilçelere sahip il hangisidir?",
           "Türkiyede petrol varlığı nedeniyle sonradan il olan şehrimiz hangisidir?",
           "Süveyş Kanalı hangi iki kıta arasındadır?",
           "Hangi ülkenin 2 farklı başkenti vardır?",
           "Dünyanın en yüksek binası hangi ülkededir?",
           "CN Kulesi Kanadanın hangi şehrindedir?",
           "Kaliforniyadaki en kalabalık şehir neresidir? Kaliforniyanın en kalabalık şehri neresidir?",
           "Hangi ülkenin İsveç ile sınırı vardır ?",
           "Palmar de Ocoa plajı nerededir?",
           "Bici bici hangi yörenin yiyeceğidir?",
           "Romanın sembolü ve İtalyanın meşhur olduğu yapı nedir?",
           "Fugiyama Dağı nerededir?",
           "Bulunan ilk maden hangisidir?",
           "Yüzölçümü Olarak Türkiyenin En büyük şehri Hangisidir?",
           "Türkiye'nin Karadeniz'deki en büyük adası hangi şehirle aynı adı taşır?",
           "Birbiriyle bağlantılı birçok dağdan oluşan yer şekline ne ad verilir?",
           "Estetik oranının en fazla olduğu ülke neresidir?",
           "Adalardan Hangisi Ayrıca Bir Ülkedir?",
           "Hangi ülke en geniş imparatorluktur? En geniş imparatorluk hangisidir?",
           "Şah hangi ülkenin geleneksel lideridir?",
           "Karayipler Denizinin anlamı nedir?",
           "Hasankeyf Hangi Şehrin İlçesidir?",
           "Hollywood hangi ABD şehrinin mahallesidir?",
           "Türkiyede en son il olan ilçe hangisidir?",
           "Doğal yangınların oluşabileceği tek gezegen hangisidir?",
           "Kurtarıcı İsanın heykeli Brezilyanın hangi şehrinde bulunmaktadır?",
           "Hangi şehrimizden petrol çıkartılmaktadır? Petrol çıkarılan şehrimiz hangisidir?",
           "Hangisi geniş yapraklı ağaçtır? Geniş yapraklı ağaç hangisidir?",
           "İtalyadaki en büyük aktif yanardağ hangisidir?",
           "Karayip bölgesindeki en büyük 2. (ikinci) ülke hangisidir?",
           "İpek ve Baharat Yolu nereden geçiyor?",
           "Bermuda Şeytan Üçgenini oluşturan 3 nokta hangileridir?",
           "Bu çöllerden hangisi dünyadaki en büyük çöldür? Dünyanın en büyük çölü hangisidir?",
           "Samsunda Amazon kadınlarının yaşadığı rivayet edilen tepenin adı nedir?",
           "Pideli köfte, cantık ve kestane şekeriyle bilinen ilimiz hangisidir?",
           "Azerbaycan hangi kıtadadır?",
           "Depremin başlangıç noktasına ne denir?",
           "Hangisi Denizli nin ilçesi değildir?",
           "ABDnin hem en kuzey, hem en batı, hem de en doğudaki eyaleti neresidir?",
           "Vezüv Yanardağı tarafından yok edilen şehir hangisidir?",
           "Hangi ülkede hayvanlara vatandaşlık hakkı verilmiştir?",
           "Hangi şehir su yollarında seyreden gondollarıyla ünlüdür?",
           "Rusya ile ABD nin arasındaki Ada nın adı nedir?",
           "Bermuda Üçgeni Hangi Okyanus Üzerindedir?",
           "Kangal cinsi köpeğiyle ünlü şehirimiz neresidir?",
           "Hangi bölgede dağlar denize dik uzanır? Dağların denize dik uzandığı bölge hangisidir?",
           "Baklavasıyla meşhur şehrimiz hangisidir?",
           "ishak paşa sarayı hangi ilimizde bulunmaktadır?",
           "Pamukkale Travertenleri hangi il sınırları içerisindedir?",
           "Bodrum ilçesi hangi ilimize bağlıdır?",
           "Hangisi Gümüşhane ilçesidir?",
           "Mozambik in bulunduğu kıta aşağıdakilerden hangisidir?",
           "Hangisi kahramanmaraş ın en büyük ilçesidir?",
           "Amazon ormanları hangi ülkededir?",
           "Hangi kanal Sina Yarımadasını anavatanı Mısırdan ayırır?",
           "Balkanların coğrafi olarak en küçük ülkesi hangisidir?",
           "Suudi Arabistan ve Mısır ülkelerinin arasındaki denizin adı nedir?",
           "Kayseri ili hangi bölgede bulunmaktadır?",
           "irlandanın başkenti neresidir?",
           "Hangi nehir dünyadaki tatlı suyun %20 sine sahiptir?",
           "Bozkır Hangi ilimize bağlı bir ilçedir?",
           "Hangisi Arap dünyasının en küçük ülkesidir?",
           "Hangi ülkenin Atlas Okyanusuna sınırı vardır?",
           "Hangisi Eski Yugoslav ülkesi değildir?",
           "Pakistan in başkenti neresidir?",
           "Hangi ülke kendisine Helen Cumhuriyeti der?",
           "Meksika sınır kapısına en yakın Amerika eyaleti aşağıdakilerden hangisidir?",
           "Hangisi farklı bir kıtadadır?",
           "Asya ve Avrupa Kıtasını birbirine bağlayan köprü hangi şehirdedir?",
           "Kıbrısın Başkenti Nedir?",
           "Türkiye de ilk kâğıt fabrikası nerede kuruldu?",
           "Dondurmasıyla ünlü ilimiz hangisidir?",
           "Akdeniz bölgesinde kaç il vardır ?",
           "Atlantik okyanusunun diğer adı hangisidir?",
           "2014 yılı itibariyle dünyanın en uzun binası nerededir?",
           "Ülkemizin en kuzeyindeki il hangisidir?",
           "Toros dağları hangi bölgemizde bulunur?",
           "Hangi şehrin 4 farklı bölgede sınırı vardır?",
           "Hangi ülkenin Türkiye ile ortak sınırı yoktur ?",
           "Hangi il iftarı daha önce yapar?",
           "Komodo ejderlerinin de yaşadığı Komodo Adası nerededir?",
           "Dünya yı yatay olarak iki eşit parçaya ayıran hayali dairesel hat nedir?",
           "Hangisinin Karadenize kıyısı yoktur?",
           "Hangi iki ülkenin nüfusu bir milyardan fazla?",
           "Afrika ve Avrupa kıtalarını bağlayan boğazın adı nedir?",
           "Ünlü Bridge köprüsü nerededir?",
           "Valencia şehri hangi ülkededir?",};
    String[] cev ={"Belçika",
            "Rusya",
            "Endonezya",
            "Arap yarımadası",
            "Boğaz vadi",
            "Amerika",
            "64",
            "Şili",
            "Yunanistan",
            "Ay",
            "Finike",
            "Hatay",
            "Dhaka",
            "Kıvrımlı Su Yolu",
            "Artvin",
            "Batman",
            "Afrika ve Asya",
            "Güney Afrika",
            "Birleşik Arap Emirlikleri",
            "Toronto",
            "Los Angeles",
            "Norveç",
            "Dominik Cumhuriyeti",
            "Adana",
            "Kolezyum",
            "Japonya",
            "Bakır",
            "Konya",
            "Giresun",
            "Sıradağlar",
            "Güney Kore",
            "Malta Adası",
            "Birleşik Krallık",
            "İran",
            "Yamyamların denizi",
            "Batman",
            "Los Angeles",
            "Düzce",
            "Dünya",
            "Rio de Janeiro",
            "Batman",
            "Meşe",
            "Etna",
            "Dominik Cumhuriyeti",
            "Asya",
            "Florida, Bermuda ve Porto Riko",
            "Sahra",
            "Amisos",
            "Bursa",
            "Asya",
            "Merkez üssü",
            "Osmaneli",
            "Alaska",
            "Pompei",
            "Hollanda",
            "Venedik",
            "Diomede",
            "Atlantik Okyanusu",
            "Sivas",
            "Ege Bölgesi",
            "Gaziantep",
            "Ağrı",
            "Denizli",
            "Muğla",
            "Kelkit",
            "Afrika",
            "Elbistan",
            "Brezilya",
            "Süveyş Kanalı",
            "Arnavutluk",
            "Kızıldeniz",
            "İç Anadolu Bölgesi",
            "Dublin",
            "Amazon Nehri",
            "Konya",
            "Bahreyn",
            "Çin",
            "Romanya",
            "İslamabad",
            "Yunanistan",
            "El Paso",
            "Mısır",
            "İstanbul",
            "Lefkoşa",
            "Yalova",
            "Kahramanmaraş",
            "8",
            "Atlas",
            "Dubai",
            "Sinop",
            "Akdeniz bölgesi",
            "Bilecik",
            "Ukrayna",
            "Bitlis",
            "Endonezya",
            "Ekvator",
            "Bolu",
            "Çin ve Hindistan",
            "Cebelitarık Boğazı",
            "Londra",
            "İspanya",};
   TextView sekran;
    Button c1,c2,c3,c4,lf,rt;
    int sorular=0;
    InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a6);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        sekran=(TextView)findViewById(R.id.soru);
        c1 = (Button)findViewById(R.id.c1);
        c2 = (Button)findViewById(R.id.c2);
        c3 = (Button)findViewById(R.id.c3);
        c4 = (Button)findViewById(R.id.c4);
        rt = (Button)findViewById(R.id.rt);
        lf = (Button)findViewById(R.id.lf);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1312048647642571/7340871040");
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        ys();
        rt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                rt.setBackgroundResource(R.drawable.r2);
                lf.setBackgroundResource(R.drawable.l1);
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(soru.length-1<=sorular)
                {
                    sorular=-1;
                }
                sorular++;
                ys();
            }
        });
        lf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                rt.setBackgroundResource(R.drawable.r1);
                lf.setBackgroundResource(R.drawable.l2);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(sorular<=0)
                {
                    sorular=soru.length;
                }
                sorular--;
                ys();
            }
        });

    }
    public void ys()
    {
        sekran.setText(soru[sorular]);
        Random rt =new Random();
        int nb=rt.nextInt(4)+1;
        switch (nb)
        {
            case 1:{ c1.setText(cev[sorular]); c1.setBackgroundResource(R.drawable.dol); break;}
            case 2:{ c2.setText(cev[sorular]); c2.setBackgroundResource(R.drawable.dol); break;}
            case 3:{ c3.setText(cev[sorular]); c3.setBackgroundResource(R.drawable.dol); break;}
            case 4:{ c4.setText(cev[sorular]);  c4.setBackgroundResource(R.drawable.dol); break;}
        }
    }
}
