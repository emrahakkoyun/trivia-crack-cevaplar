package com.smartinsoft.trvCrack;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Splash extends AppCompatActivity {
    Handler hd;
    int count=2;
    Boolean tb=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        hd= new Handler();
        Thread  t = new Thread(new Runnable() {
            @Override
            public void run() {
            while(tb==true)
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                hd.post(new Runnable() {
                    @Override
                    public void run() {
                        if(count>0)
                        {

                        }
                        else{
                            tb=false;
                        }
                        count--;
                    }
                });

            }
                if(tb==false)
                {
                    startActivity(new Intent(Splash.this,Categori.class));
                }
            }
        });
        t.start();

    }
}
