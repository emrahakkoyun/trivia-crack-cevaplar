package com.smartinsoft.trvCrack;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Random;

public class A2 extends AppCompatActivity {
    String[] soru={"Metin Oktayın forma numarası kaçtır?",
            "Şampiyonlar Ligini 5 kez üst üste kazanan tek takım hangisidir?",
            "Lionel Messi nerede doğdu?",
            "Hangi sporda kırmızı, beyaz, yeşil ve sarı renkli bayraklar kullanılır?",
            "2000deki Paralimpik Oyunlarında sonradan 10 oyuncusunun zihinsel engelli olmadığı anlaşılan hangi ülkenin basketbol takımı altın madalya kazanmıştır?",
            "Türkiye süper liginde kaç takım vardır?",
            "9 dakikada 5 gol atarak Guiness Rekorlar Kitabına adını yazdıran futbolcu kimdir?",
            "Bir basketbol takımında, en uzun boylu olması beklenen kimsenin pozisyonu kimdir?",
            "Kırmızı şeytan hangi İngiliz futbol takımının amblemidir?",
            "Sporda en büyük top hangisidir?",
            "Dünya Kupasındaki ülke sayısı kaçtır?",
            "Voleybolda altın set kaç sayıda biter?",
            "Basketbolda alley-oop kaç puandır?",
            "Hollanda milli takımının taraftar ve oyuncularının lakabı nedir?",
            "Futbolda top hakeme çarparsa ne olur?",
            "Japonyada hangi spor kılıçla yapılır?",
            "Türk Telekom Arena Spor Kompleksinde ilk golü kim atmıştır?",
            "Transfer ücreti en pahalı olan futbolcumuz kimdir?",
            "Hangi terim okçuluk sporunda yer alır?",
            "Madagaskar'da 2002'de önceki maçta son dakika penaltısıyla şampiyonluğu kaçıran futbol takımı, hakemi protesto etmek için sonraki maç boyunca kendi kalesine kaç gol atmıştır?",
            "10000in (on bin) üzerinde oyun kaybetmiş tek takım hangisidir?",
            "Fenerbahçe nin en çok gol atan yabancı oyuncusu kimdir?",
            "ABD (Amerika Birleşik Devletleri)nin ulusal sporu nedir?",
            "Gol pozisyonu olmayan spor hangisidir?",
            "NBA'de şampiyon olan takım oyuncularına hangi takı armağan edilir?",
            "Teniste başlamak için atılan vuruşa ne denir?",
            "Hangisi en popüler ata sporlarımızdan birisidir?",
            "1. Avrupa Olimpiyat Oyunları hangi ülkede yapılmıştır?",
            "Basketbolda uzatma periyotları kaç dakika oynanır?",
            "Finlandiya'da 2000 yılından beri her yıl hangisini fırlatmak üzere bir dünya şampiyonası düzenlenmektedir?",
            "Hentbolda top elinize geldiğinde topu sektirmeden önce en fazla kaç adım atabilirsiniz?",
            "Gençler ve büyüklerde ilk amatör boks şampiyonumuz kimdir?",
            "Şampiyonlar Ligi kupasını en çok kazanan takım hangisidir?",
            "Futbolda yedek kulübesinde en çok kaç yedek oyuncu bulundurulabilir ?",
            "Pele kariyerine hangi takımda başlamıştır?",
            "Hangisi raket ile oynanır?",
            "Aynı gün hem profesyonel futbol hem de profesyonel basketbol maçına çıkan sporcu kimdir?",
            "Olimpiyat sporlarından hangisinde sadece erkekler yarışır?",
            "Jimnastikte bir jürinin verebileceği en yüksek puan kaçtır?",
            "Hangi Spor dalında dikişli top, sopa ve 4 köşe vardır?",
            "Hangisi Tenis Terimi Değildir?",
            "Hangi spor dalında sopa kullanılmaz?",
            "Michael Jordan kaç kere NBA efsanesi ünvanını kazanmıştır?",
            "Pique nin eşinin adı nedir?",
            "Teakwando müsabakalarında hakemler hangi renk kuşağa sahip olmalı?",
            "Türkiye süper liginde en fazla gol kralı olan futbolcu aşağıdakilerden hangisidir?",
            "Kelebek gibi uçar, arı gibi sokarım sözüyle ünlü boksör kimdir?",
            "Hangisi Ülkemizin Futbol Süper Lig Sponsorudur?",
            "Aşağıdakilerden hangisi voleybol taktiği değildir?",
            "Olimpiyat halkalarından hangi renkte olan Afrika kıtasını temsil eder?",
            "Hangisi maça çıkmadan önce bir sporcuya yapılan testtir?",
            "Meksikada kaç tane dünya kupası düzenlenmiştir?",
            "Tenis sahasına ne ad verilir?",
            "Hangi dövüş sanatında rekabet yoktur?",
            "Alpinizm hangi spor dalıyla ilgilidir?",
            "Olimpiyat oyunlarında 3. sırayı alan kişiye hangi madalya verilmektedir?",
            "Zamanına damgasını vurmuş en ünlü nba oyuncularından Micheal Jordanın oynadığı ve bir sürü şampiyonluğa imza attığı takım hangisidir?",
            "Hangi futbolcu Karpatların Maradonası diye anılmaktadır?",
            "Hangi ülke, erkekler 4x100 metre atletizmde dünya rekoru kırmıştır?",
            "Hangi futbolcu fenerbahçe de forma giymemiştir?",
            "Hangi Şarkıcı 2010 Dünya Futbol Kupası Şarkısını Seslendirdi?",
            "Türkiyede oynanılmamasına rağmen çok satan spor ürünü hangisidir?",
            "Samsunspor un renkleri hangisidir?",
            "Dünya tarihinde en çok para kazanan sporcu hangi dalda başarılıdır?",
            "James Rodriguez hangi takımda forma giymektedir?",
            "Hangi Spor Olimpik Değildir?",
            "Aşağıdakilerden Hangisi TrabzonSpor un Renkleridir ?",
            "Aşağıdaki Futbolculardan Hangisinin Lakabı Kral dır?",
            "NBA e Amerika dışından katılan takım hangisidir?",
            "İspanyol Real Madrid futbol kulübü hangi yıl kurulmuştur?",
            "Hangisi Bir italyan Takımıdır?",
            "2014 Altın futbol ayakkabı ödülünü kim almıştır?",
            "Asrın Güreşcisi Olarak bilinen olimpiyat şampiyonu milli güreşçimiz kimdir?",
            "Hangi futbol takımının renkleri siyah ve beyaz değildir?",
            "Hangisi basketbol terimidir?",
            "Hangisi milli koşucudur?",
            "Hangi takım UEFA kupasını hiç mağlup olmadan kazanmıştır?",
            "Tiger Woods hangi sporla ilgilenir?",
            "Hangisi büyük kas grubuna girer?",
            "Boğaz ın Boğası lakaplı Türk ağır sıklet boksörü kimdir?",
            "Hangisi farklı bir ligde oynamaktadır?",
            "Rok hangi sporda bir terimdir?",
            "Bowling oyununda tek seferde tüm kukaları devirmenin adı nedir?",
            "Baba lakablı efsanevi Beşiktaş forması giymiş futbolcu kimdir?",
            "Hangisi kalecisi olmayan bir spordur?",
            "Fenerbahçe nin unutulmaz futbolcusu Lefter in soyadı nedir?",
            "Hangi futbol takımı 4 büyükler diye nitelendirilen takımlar dışında şampiyonluk yaşamıştır?",
            "Beşiktaşın sattığı en pahallı futbolcu kimdir?",
            "Hangi sporda kullanılan top daha ufaktır?",
            "İlk Dünya Kupasını hangi ülke kazanmıştır?",
            "Queens Park Rangers hangi şehirin takımıdır?",
            "Hangisi futbol terimi değildir?",
            "Hangisi voleybol tekniklerinden degildir?",
            "4 kez Supersport şampiyonu olan Türk motorcumuz kimdir?",
            "Hangi sporu yaparken kılıç kullanırız?",
            "Türkiye nin 2002 Dünya Kupası nda Senegal e karşı oynadığı maçta altın golü kim atmıştır?",
            "Potayı kıran basketbol oyuncusu kimdir?",
            "Hangi kuruluş diğerlerinden yaşça büyüktür?",
            "Hangi Futbolcu Türkiye Futbol Liginde Forma Giymektedir?",
            "Hangisi tenis ile benzer bir spordur?",
            "2008 Avrupa Şampiyonası nın şampiyonu hangi ülke olmuştur?",};
    String[] cev ={"10",
            "Real Madrid",
            "Arjantin",
            "Araba yarışı",
            "İspanya",
            "18",
            "Robert Lewandowski",
            "Pivot",
            "Manchester United",
            "Basketbol topu",
            "32",
            "15",
            "2",
            "Portakal",
            "Hakem sahanın bir parçasıdır.",
            "Kendo",
            "Servet Çetin",
            "Arda Turan",
            "Sadak",
            "149",
            "Philadelphia Phillies",
            "Alex de Souza",
            "Beyzbol",
            "Basketbol",
            "Şampiyonluk yüzüğü",
            "Servis",
            "Güreş",
            "Azerbaycan",
            "5",
            "Cep telefonu",
            "3",
            "Sinan Şamil Sam",
            "Real Madrid",
            "7",
            "Santos",
            "Tenis",
            "Can Bartu",
            "Kulplu beygir",
            "10",
            "Beyzbol",
            "Pas",
            "Badminton",
            "6",
            "Shakira",
            "Siyah",
            "Tanju Çolak",
            "Muhammet Ali",
            "Spor Toto",
            "8-8-8",
            "Siyah",
            "Doping testi",
            "2",
            "Kort",
            "Aikido",
            "Dağcılık",
            "Bronz",
            "Chicago Bulls",
            "Gheorghe Hagi",
            "Jamaika",
            "Simao",
            "Shakira",
            "Beyzbol sopası",
            "Kırmızı Beyaz",
            "Golf",
            "Real Madrid",
            "Karate",
            "Bordo-Mavi",
            "Hakan Şükür",
            "Boston Celtics",
            "1902",
            "Torino",
            "C.Ronaldo",
            "Hamza Yerlikaya",
            "Borussia Dortmund",
            "Steps",
            "Merve Aydın",
            "Galatasaray",
            "Golf",
            "Göğüs",
            "Sinan Şamil Sam",
            "O.Lyon",
            "Satranç",
            "Strike",
            "Baba Hakkı",
            "Amerikan Futbolu",
            "Küçükandonyadis",
            "Bursaspor",
            "Demba Ba",
            "Hentbol",
            "Uruguay",
            "Londra",
            "Tac",
            "Vale",
            "Kenan Sofuoğlu",
            "Eskrim",
            "İlhan Mansız",
            "Shaquille o neal",
            "Beşiktaş",
            "Mario Gomez",
            "Badminton",};
    TextView sekran;
    Button c1,c2,c3,c4,lf,rt;
    int sorular=0;
    InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        sekran=(TextView)findViewById(R.id.soru);
        c1 = (Button)findViewById(R.id.c1);
        c2 = (Button)findViewById(R.id.c2);
        c3 = (Button)findViewById(R.id.c3);
        c4 = (Button)findViewById(R.id.c4);
        rt = (Button)findViewById(R.id.rt);
        lf = (Button)findViewById(R.id.lf);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1312048647642571/8957205047");
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        ys();
        rt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                rt.setBackgroundResource(R.drawable.r2);
                lf.setBackgroundResource(R.drawable.l1);                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }

                if(soru.length-1<=sorular)
                {
                    sorular=-1;
                }
                sorular++;
                ys();
            }
        });
        lf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                rt.setBackgroundResource(R.drawable.r1);
                lf.setBackgroundResource(R.drawable.l2);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(sorular<=0)
                {
                    sorular=soru.length;
                }
                sorular--;
                ys();
            }
        });

    }
    public void ys()
    {
        sekran.setText(soru[sorular]);
        Random rt =new Random();
        int nb=rt.nextInt(4)+1;
        switch (nb)
        {
            case 1:{ c1.setText(cev[sorular]); c1.setBackgroundResource(R.drawable.dol); break;}
            case 2:{ c2.setText(cev[sorular]); c2.setBackgroundResource(R.drawable.dol); break;}
            case 3:{ c3.setText(cev[sorular]); c3.setBackgroundResource(R.drawable.dol); break;}
            case 4:{ c4.setText(cev[sorular]);  c4.setBackgroundResource(R.drawable.dol); break;}
        }
    }
}
