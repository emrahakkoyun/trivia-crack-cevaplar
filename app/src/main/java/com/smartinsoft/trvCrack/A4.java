package com.smartinsoft.trvCrack;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Random;

public class A4 extends AppCompatActivity {
    String[] soru={"Hangi iki insan arabayı icat etti ?",
            "Dünyanın en uzun boylu insanı kimdir ?",
            "Kenan Evren darbesi kaç yılında yapılmıştır?",
            "İzmirin işgali sırasında Yunanlılara ilk ateş eden kimdir?",
            "İslamiyet öncesi Türk devletlerinde Orta Asyada bulunan kutsal kabul edilen şehir hangisidir?",
            "Nelson Mandela ne kadar süre hapis yatmıştır?",
            "İncildeki en son kelime nedir? İncilin en son kelimesi nedir?",
            "Taş devrinde insanlar hangi 2 avantajı kullanmışlardır?",
            "Türklerde ilk düzenli orduyu kuran lider hangisidir?",
            "Amerika Birleşik Devletleri bayrağında kaç yıldız ve kaç şerit bulunmaktadır?",
            "Tek başına, hiçbir silah kullanmadan bir savaş kazanan kişinin adı nedir?",
            "Hangi Romalı imparator kendi senatörleri tarafından öldürüldü?",
            "Güneş, hayvanlar gibi doğaya tapan insanlara ne ad verilir?",
            "Osmanlıda yapılan ilk 6 minareli cami aşağıdakilerden hangisidir?",
            "Rönesans adamı kimdir?",
            "Savaş esnasında insanların kırmızı pelerin giymesiyle meşhur antik Yunan kenti hangisidir?",
            "Meşrutiyet karşıtı ayaklanma aşağıdakilerden hangisidir?",
            "Halifeliği getiren padişah kimdir?",
            "Yunan mitolojisinde evlilik ve aile tanrıçası Herayı sembolize eden hayvan hangisidir?",
            "İlk gözlük 1280 yılında nerede yapılmıştır?",
            "Denizcilik faaliyetleri ile uğraşan ilk Türk Devleti hangisidir?",
            "Anadolunun ilk beyliği hangisidir?",
            "Yıldırım harekatı diye bilinen savaş türünü kim başlatmıştır?",
            "Tarihteki en eski anıtlar hangi uygarlığa aittir?",
            "Peygamber efendimizin süt kardeşinin adı nedir?",
            "Tarihte şarabı yasaklayan padişah olarak tanınan padişah hangisidir?",
            "Osmanlı Devleti ilk dış borcu nereden almıştır?",
            "Hz. İsa nerede çarmıha gerildi?",
            "Mezopotamyanın kelime anlamı nedir?",
            "Arjantinli devrimci Che Guevara nerede vefat etmiştir?",
            "Kanuni Sultan Süleymanın karısı kimdir?",
            "2.Dünya Savaşında hangi ülke panzer isimli tanklarına güveniyordu ?",
            "Hun'ların günümüzdeki varisleri kimlerdir?",
            "En büyük deprem 1989 yılında Amerikada nerede olmuştur?",
            "Barbaroza Operasyonu hangi 2 ülke arasında gerçekleşmiştir?",
            "Hangisi Moğol İmparatorluğunun bölünmesinden sonra ortaya çıkan devletlerden değildir?",
            "Gılgamış Destanı nerede doğmuştur?",
            "Roma İmparatorluğunun devamı olarak görülen devlet hangisidir?",
            "Musa İsrailoğullarını hangi ülkeden çıkardı?",
            "Cengiz Han Yasalarını kim hazırlamıştır?",
            "I. Dünya Savaşında hangi ülke tarafsız kaldı?",
            "Yunan mitolojisinde ilk kadın kimdir?",
            "Osmanlıyı Fetret Devrine sokan filleriyle ün salmış Türk hükümdarı kimdir?",
            "Hangisi İstanbula Osmanlı Devleti zamanında verilen isimlerden biri değildir?",
            "Mısır kraliçesi Kleopatraya aşık olan kimdir?",
            "İstiklal Marşının son kelimesi nedir?",
            "Kral Arthur hangi krallığın başındaydı?",
            "SSCB dağıldıktan sonra kaç ülke bağımsız oldu?",
            "Rodosu fetheden Osmanlı padişahı hangisidir?",
            "Eflak Beyi 3. Vlad Tepeşin favori işkencesi neydi?",
            "Kral 8. Henry eşlerinden ötürü ne sebeple mutsuzdu?",
            "Hangi başkan 1961de Kübada başarısızlıkla sonuçlanan Bay of Pigs istilasının emrini vermiştir?",
            "Hangi savaşta Yunanlar ve Spartalılar beraber savaştı?",
            "Hangisi Amerikan Gotik Edebiyatının öncü yazarlarındandır?",
            "Tarihteki hangi savaşçı ırk gerçek hayatta kask kullanmadıkları halde boynuzlu kasklarla tasvir edilmiştir?",
            "Yunan mitolojisine göre demircilik ile uğraşan tanrı hangisidir?",
            "30 gümüş için Hz. İsaya ihanet eden havari kimdir?",
            "2. Dünya Savaşında en son teslim olan ülke hangisidir?",
            "İskoçya kaç yılında bağımsızlığını ilan etmiştir?",
            "Mezopotamya daki ilk uygarlık hangisidir?",
            "İlk Türk devleti hangi ülkeyle komşuydu?",
            "Kronolojik olarak ilk sıradaki hangisidir?",
            "Hangi kültür daha eskidir?",
            "Osmanlı İmparatorluğunda devşirilen çocukların acemi ocağını bitirdikten sonra aralarından bazı seçilen çocukların sarayda eğitim gördükleri yer neresidir?",
            "Eski zamanlarda hangi Roma imparatoru bir şehre adını verip onu Bizans ve Roma İmparatorluğunun başkenti ilan etmiştir?",
            "Hangisi hala görev başında olan hükümdarlardan en uzun süre başta kalan hükümdar ünvanıyla Guiness rekorlar kitabına girmiştir?",
            "Osmanlı Devletindeki ilk resmi gazetenin adı nedir?",
            "Hangi politik sistem 1920lerde Amerikan borsasının yükselmesini sağladı?",
            "1989da yıkılan ve parçaları anı olarak satışa sunulan 46 km uzunluktaki yapı hangisidir?",
            "Hangisi 12 Olimposludan biri değildir?",
            "Atatürk ün çok sevdiği atının ve köpeğinin isimleri hangi şıkta doğru olarak verilmiştir?",
            "Amerika iç savaşı kaç yıl sürdü?",
            "Türkiye Cumhurbaşkanlığı armasındaki yıldızlar neyi temsil etmektedir?",
            "Guiness rekorlar kitabına göre en kısa savaş ne kadar sürmüştür?",
            "Hangi savaşta bir Türk devleti kurulurken diğer bir Türk devleti yıkılmıştır?",
            "Hangisi Yeni Çağı bitiren olaydır?",
            "Amerika Birleşik devletlerinin 43. Başkanı Kimdir? ABD",
            "Abraham Lincoln kim tarafından öldürüldü?",
            "Hangisi 1.dünya savaşında Osmanlının savaştığı cephelerden biridir?",
            "Hangisi Orhun Anıtı Değildir?",
            "Kamikaze Pilotları Hangi Ülkeden Çıkmıştır?",
            "Dünya emek ve işçiler günü ne zamandır?",
            "Hangisi Türk halklarından biri değildir?",
            "I. Dünya Savaşından sonra yenilen ülkelerin durumunu görüşmek üzere hangi konferans toplanmıştır?",
            "1. Dünya Savaşının ardından Almanya ile imzalanan barış anlaşması hangisidir?",
            "Mezopotamya'daki ilk uygarlık hangisidir?",
            "Hitit devletinin başkenti neresidir?",
            "Hangisi Soğuk Savaş dönemi Doğu Blok ülkelerindendir?",
            "Aşağıdaki savaşlardan hangisinin çıkış sebebi bir kadın uğrunadır?",
            "Sovyetler Birliği hangi yıl dağıldı?",
            "Kafasına 3 kurşun sıkılarak öldürülen ünlü din adamı Rasputin nerelidir?",
            "Hangisi Barbaros Hayrettin Paşanın kazandığı deniz zaferidir?",
            "Devşirme sistemi nedir?",
            "Osmanlı Devleti ile Haçlılar arasındaki ilk savaş hangisidir?",
            "Suikaste uğrayınca Birinci Dünya Savaşı başlayan Avusturya-Macaristan Arşidükünün adı nedir?",
            "Mustafa Kemal Atatürk ün Naaşı Anıtkabirden Önce Neredeydi?",
            "Dünyanın Tarih Öncesi dönemi ne olarak da adlandırılır?",
            "Aşağıdakilerden hangisi Antik Çin'de kadınsı güzelliğin belirtisidir?",
            "Atatürkün ilk görev yeri neresidir?",
            "Türklerin kullandığı ilk alfabe hangisidir?",
            "Nasyonal ... Alman Partisi boşluğa aşağıdakilerden hangisi gelmelidir?",
            "Hangi ünlü sanatçı bir hayranı tarafından öldürülmüştür?",
            "Hangi Osmanlı padişahı yeniçeriler tarafından öldürülerek, bu şekilde ölen ilk padişah olmuştur?",
            "Eski Mısır'ın son firavunu aşağıdakilerden hangisidir?",
            "Hangi Türk destanında Türkler bir dağ içinde yıllar boyunca gizlenmişlerdir?",
            "20. yüzyılın ilk günü hangisidir?",
            "2015 yılı itibariyle Avrupa Birliği kaç ülkeden oluşmaktadır?",
            "Hangi uygarlık Bizans İmparatorluğu'nun yıkılmasında etkili olmuştur?",
            "Osmanlı Devleti Edirneyi hangi savaş ile almıştır?",
            "Hristiyanların ibadet etmek için kullandığı ilk bazilika kimin tarafından bağışlandı?",
            "Osmanlı Devletinde devlet işlerinin görüşüldüğü meclise ne ad verilirdi?",
            "Hangi Yunan tanrısı bütün tanrıların tanrısıdır?",
            "Hangi ülke savaş uçakları için ilk kez jet motorunu geliştirmiştir?",
            "Hangisi avrupa yakasında değildir?",
            "Hangisi Hz.Muhammed (sav) in kızlarından değildir?",
            "Roma İmparatorluğunun son zamanlarında İstanbulun ismini kim değiştirmiştir?",
            "Hangisi sümerlerde şehir ismi değildir?",
            "Yük gemilerinden alınan ücrete ne denir?",
            "İkinci dünya savaşı sırasında Japonya ya kaç adet nükleer bomba atışmıştır?",
            "Osmanlı Devletinde ilk kez Yeniçeri sayımı yapan padişah kimdir?",
            "Hangi uygarlık Bizans İmparatorluğu nun yıkılmasında etkili olmuştur?",
            "Fatih Sultan Mehmet in mahlası nedir?",
            "İlk kadın bakanımız kimdir?",
            "Hangi kültürde güneşin her gün doğması için insanların kurban edilmesinin gerekli olduğuna inanılır?",
            "Anadolu ya sefer düzenleyen ilk Türk devleti hangisidir?",
            "II. Dünya savaşı sırasında, aşağıdaki ülkelerden hangisi Almanya tarafından istila edilmedi?",
            "National Geographic araştırmalarına göre; dünyada ırkı en fazla devam eden kişi kimdir?",
            "Hangisi Anıtkabirin mimarlarındandır?",
            "Kuran a göre Allah ın kaç ismi vardır?",
            "Veni vidi vici geldim gördüm yendim sözü kime aittir?",
            "Bunlardan hangisi ilk önce icat edilmiştir?",
            "Askeri Birliklerin içinde en üst mevki hangisidir?",
            "Osmanlı Devletinde babasını tahttan indirerek tahta çıkan ilk padişah kimdir?",
            "Dün dündür bugün bugündür sözünü söyleyen devlet büyüğü kimdir?",
            "Truva Savaşı nda savaşan bu kahramanlardan hangisi Truvalıdır?",
            "Hangisi İstanbul fethedildikten sonra kiliseden camiye dönüştürülmüştür?",
            "İlk başlarda korsan iken daha sonra Osmanlı da Kaptan-ı Deryalığa getirilen denizci kimdir ?",
            "Anıtkabir, Ankaranın hangi semtinde bulunmaktadır?",
            "İngiltere deki Anglikan kilisesini kim inşa ettirmiştir?",
            "Amerika nın Anayasası nın ilk maddesi ne ile ilgilidir?",
            "Hangisi Mustafa Kemal in ilkelerinden değildir?",
            "Hangisi Saka Destanıdır?",
            "Kennedy kardeşlerden hangisi Teddy ismi altında tanınıyordu?",
            "Günümüz dilbilimcileri, Rosetta Taşı olarak bilinen yazıtı hangi dili anlamakta kullanmıştır?",
            "Federal Almanya Cumhuriyeti nin ambleminde hangi hayvan var?",
            "Açık kapı politikasını hangi ülke kurmuştur?",
            "Süveyş Kanalı ilk olarak kimin zamanıda açılmaya başlandı?",
            "Pantolon giyen ilk kadın olarak genel itibar sahibi olan kişi kimdir?",
            "Türklerin geleneksel savaş taktiğinin adı nedir?",
            "Türkiye cumhuriyeti nin dokuzuncu cumhurbaşkanı kimdir?",
            "Türkiye nin Kurucusu Kimdir?",
            "Hangisi halk hikayeleri ile efsaneleşen aşıklardan değildir?",
            "Osmanlı Devleti nin Afrika da kaybettiği ilk toprak parçası neresidir?",
            "Hangi barış anlaşması Almanya ile yapılmıştır?",};
    String[] cev ={"Karl F. Benz ve Gottlieb Daimler",
            "Sultan Kösen",
            "1980",
            "Hasan Tahsin",
            "Ötüken",
            "27 yıl",
            "Amin",
            "Avlanma ve tarım",
            "Mete Han",
            "50 yıldız 13 şerit",
            "Gandhi",
            "Julius Caesar",
            "Paganlar",
            "Sultan Ahmet Camii",
            "Leonardo da Vinci",
            "Sparta",
            "31 Mart olayı",
            "Yavuz Sultan Selim",
            "Tavus kuşu",
            "İtalya",
            "Çaka Beyliği",
            "Saltuklular",
            "Adolf Hitler",
            "Mısır İmparatorluğu",
            "Şeyma",
            "4. Murad",
            "Fransa",
            "Kudüs, İsrail",
            "Orta Nehir Yurdu",
            "Venezuela",
            "Hürrem",
            "Almanya",
            "Moğollar",
            "San Francisco, Ca",
            "Nazi Almanyası ve Sovyetler",
            "Timur",
            "Mezopotamya",
            "Bizans İmparatorluğu",
            "Mısır",
            "Moğollar",
            "Belçika",
            "Pandora",
            "Timur",
            "Cihannüma",
            "Julius Caesar",
            "İstiklal",
            "Camelot",
            "15",
            "Kanuni Sultan Süleyman",
            "Kazığa oturtmak",
            "Ona hiç erkek evlat veremedikleri için",
            "John F. Kennedy",
            "Pers savaşı",
            "Edgar Allan Poe",
            "Vikingler",
            "Hephaistos",
            "Judas",
            "Japonya",
            "İskoçya, bağımsız değildir",
            "Sümerler",
            "Çin",
            "TBMMnin açılışı",
            "Antik Mısır",
            "Enderun mektebi",
            "Constantine",
            "Tayland kralı",
            "Takvim-i Vekayi",
            "Kapitalizm",
            "Berlin Duvarı",
            "Hera",
            "Sakarya ve Foks",
            "4",
            "Tarihteki Türk devletlerini",
            "45 dakika",
            "Dandanakan",
            "Fransız Devrimi",
            "George Walker Bush",
            "John Wilkes Booth",
            "Kanal",
            "Thomsirvan",
            "Japonya",
            "1 mayıs",
            "Halhalar",
            "Paris Barış Konferansı",
            "Versay",
            "Sümerler",
            "Hattuşaş",
            "Türkiye",
            "Truva",
            "1991",
            "Rusya",
            "Preveze",
            "Hıristiyan çocukları müslümanlaştırma",
            "Sırpsındığı",
            "Franz Ferdinand",
            "Etnografya Müzesi",
            "Taş Devri",
            "Küçük ayaklar",
            "Şam",
            "Göktürk alfabesi",
            "Sosyalist",
            "John Lennon",
            "Genç Osman",
            "7. Kleopatra",
            "Ergenekon destanı",
            "1 Ocak 1901",
            "28",
            "Osmanlı",
            "Sırpsındığı savaşı",
            "Constantine",
            "Divan-ı Hümayun",
            "Zeus",
            "Almanya",
            "Beylerbeyi Sarayı",
            "Hacer",
            "Konstantin",
            "Polis",
            "Navlun",
            "2 Bomba",
            "I. Abdülhamit",
            "Osmanlı",
            "Fuat",
            "Türkan Akyol",
            "Aztek",
            "Kaçar Türkleri",
            "İsveç",
            "Cengiz Han",
            "Emin halid onat",
            "99",
            "Antonious",
            "Barut",
            "Ordu Generali",
            "Yavuz Sultan Selim",
            "Süleyman Demirel",
            "Hektor",
            "Sultan Ahmet Camii",
            "Barbaros Hayrettin Paşa",
            "Anıttepe",
            "Thomas III",
            "Yasa koyucu madde",
            "Demokratikcilik",
            "Alp Er Tunga Destanı",
            "Edward",
            "Mısır Hiyeroglifi",
            "Kartal",
            "Amerika birleşik devletleri",
            "Sokullu Mehmet Paşa",
            "Elizabeth Smith Miller",
            "Turan Taktiği",
            "Süleyman Demirel",
            "Mustafa Kemal Atatürk",
            "Mira ile Yaman",
            "Cezayir",
            "Versailles (Versay)",};
    TextView sekran;
    Button c1,c2,c3,c4,lf,rt;
    int sorular=0;
    InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a4);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        sekran=(TextView)findViewById(R.id.soru);
        c1 = (Button)findViewById(R.id.c1);
        c2 = (Button)findViewById(R.id.c2);
        c3 = (Button)findViewById(R.id.c3);
        c4 = (Button)findViewById(R.id.c4);
        rt = (Button)findViewById(R.id.rt);
        lf = (Button)findViewById(R.id.lf);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1312048647642571/2910671449");
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        ys();
        rt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                rt.setBackgroundResource(R.drawable.r2);
                lf.setBackgroundResource(R.drawable.l1);
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(soru.length-1<=sorular)
                {
                    sorular=-1;
                }
                sorular++;
                ys();
            }
        });
        lf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c1.setBackgroundResource(R.drawable.bos);
                c2.setBackgroundResource(R.drawable.bos);
                c3.setBackgroundResource(R.drawable.bos);
                c4.setBackgroundResource(R.drawable.bos);
                rt.setBackgroundResource(R.drawable.r1);
                lf.setBackgroundResource(R.drawable.l2);
                c1.setText("");
                c2.setText("");
                c3.setText("");
                c4.setText("");
                if(sorular%10==0)
                {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                if(sorular<=0)
                {
                    sorular=soru.length;
                }
                sorular--;
                ys();
            }
        });

    }
    public void ys()
    {
        sekran.setText(soru[sorular]);
        Random rt =new Random();
        int nb=rt.nextInt(4)+1;
        switch (nb)
        {
            case 1:{ c1.setText(cev[sorular]); c1.setBackgroundResource(R.drawable.dol); break;}
            case 2:{ c2.setText(cev[sorular]); c2.setBackgroundResource(R.drawable.dol); break;}
            case 3:{ c3.setText(cev[sorular]); c3.setBackgroundResource(R.drawable.dol); break;}
            case 4:{ c4.setText(cev[sorular]);  c4.setBackgroundResource(R.drawable.dol); break;}
        }
    }
}
